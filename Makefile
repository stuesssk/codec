CPPFLAGS+=-Wall -Wextra -Wpedantic
CPPFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal
CPPFLAGS+=-Waggregate-return -Winline

CFLAGS+=-std=c11


# Source Alex Dow, who sourced Liam Echlin
BINS=decode encode


all: $(BINS)
encode: LDLIBS+=-lm
decode: decode.o zergdata.o
encode: encode.o encodefunc.o

.PHONY: clean debug profile

debug: CFLAGS+=-g
debug: all

profile: CFFLAGS+=-pg
profile: LDFALGS+=-pg
profile: all

clean:
	$(RM) $(BINS) *.o
