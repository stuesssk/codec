/**************************************************************************
      Classification: U/FOUO
*//**
    @file
    @brief Program used to decode Zerg Psychic CAPtures (PCAP) of network 
           traffic. And to deisplay all relevent data pertaining to the 
           Zergs.

    Program will accept and decode a Zerg Psychic CAPtures (PCAP) of network 
    traffic. The program will determine the type of Zerg packet that was
    captured and display the data that is encoded into that type of 
    zerg packet.
***************************************************************************/
#include <arpa/inet.h>

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <sysexits.h>
#include <sys/stat.h>

#include "struct.h"
#include "zergdata.h"

// Size of pcap File Header
enum
{PCAP_FILE_HEADER = 24 };

// Size of pcap File Header
enum
{PCAP_PACKET_HEADER = 16 };

// Size of Ethernet Frame
enum
{ETHERNET_FRAME = 14 };

// Size of IPv4 Header
enum
{IPV4_HEADER = 20 };

// Size of UDP Header
enum
{UDP_HEADER = 8 };

int
main(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s <file1>\n", argv[0]);
        return EX_IOERR;
    }

    struct capHeader fileHeader = { 0 };
    struct packetHeader pHead = { 0 };
    struct ethFrame ethernet = { 0 };
    union
    {
        struct ipv4Header ipv4;
        struct ipv6Header ipv6;
    } ipheader;
    //struct ipv4Header ipv4 = { 0 };
    struct udpHeader udp = { 0 };
    struct zergHeader zerg = { 0 };

    // Calculating file size in bytes taken from:
    // stackoverflow.com/question/238603/how-can-i-get-a-file-size-in-c
    struct stat fileStat;

    stat(argv[1], &fileStat);
    int size = fileStat.st_size;
    // Ensuring file is not empty (size == 0)
    if (size == 0)
    {
        fprintf(stderr, "Input File is of size 0\n");
        return EX_NOINPUT;
    }

    FILE *fp = fopen(argv[1], "rb");
    if (!fp)
    {
        fprintf(stderr, "Unable to open file‽\n");
        return EX_NOINPUT;
    }


    // PCAP file has fixed length of 94 bytes before a zerg payload
    // any PCAP with less than 94 bytes is an invalid packet
    if (size < 94)
    {
        fprintf(stderr, "PCAP file is smaller than minimum"
                        " PCAP size of 98 Bytes.");
        fclose(fp);
        return EX_IOERR;
    }
    // subtracting 24 bytes (size of PCAP File header) to have running
    // total of bytes that are physically left in file
    size -= sizeof(fileHeader);

    // Read in pcap File Header (only once per file)
    int read = fread(&fileHeader, PCAP_FILE_HEADER, 1, fp);
    if (read < 1)
    {
        fprintf(stderr, "Could not read pcap File Header");
        fclose(fp);
        return EX_IOERR;
    }

    // Ensure file is actually a PCAP file
    if (fileHeader.fileID != 0xa1b2c3d4)
    {
        fprintf(stderr, "Invalid PCAP file\n");
        fclose(fp);
        return EX_DATAERR;
    }

    while (true)
    {
        // Read in pcap Packet Header
        read = fread(&pHead, PCAP_PACKET_HEADER, 1, fp);
        if (read < 1)
        {
            fprintf(stderr, "Could not read pcap Packet Header");
            fclose(fp);
            return EX_IOERR;
        }
        // Bytes remaining in file
        size -= sizeof(pHead);

        // Read in Ethernet frame
        read = fread(&ethernet, ETHERNET_FRAME, 1, fp);
        if (read < 1)
        {
            fprintf(stderr, "Could not read Ethernet Frame");
            fclose(fp);
            return EX_IOERR;
        }
        // Bytes remaining in file
        size -= sizeof(ethernet);

        // Checking ethernet frame's ethernet type to determine if using 
        // IPv4 of IPv6 header and reading in the apporpriate headers type.
        if (ntohs(ethernet.ethType) == 0x0800)
        {
            printf("IP Version: IPv4\n");
            read = fread(&ipheader.ipv4, IPV4_HEADER, 1, fp);
            if (read < 1)
            {
                fprintf(stderr, "Could not read Ipv4 Header");
                fclose(fp);
                return EX_IOERR;
            }
            // checking for length mismatch in ipv4 length to bytes
            // total bytes reminaing in file
            if (size < ntohs(ipheader.ipv4.length))
            {
                fprintf(stderr, "Packet length mismatch."
                                " Not enough bytes left in file\n");
                fclose(fp);
                return EX_DATAERR;
            }
            // Bytes remaining in file
            size -= sizeof(ipheader.ipv4);
        }
        else if (ntohs(ethernet.ethType) == 0x86DD)
        {
            printf("IP Version: IPv6\n");
            read = fread(&ipheader.ipv6, 40, 1, fp);
            if (read < 1)
            {
                fprintf(stderr, "Could not read Ipv6 Header");
                fclose(fp);
                return EX_IOERR;
            }
            // checking for length mismatch in ipv4 length to bytes
            // total bytes reminaing in file
            if (size < ntohs(ipheader.ipv6.payloadLen))
            {
                fprintf(stderr, "Packet length mismatch."
                                " Not enough bytes left in file\n");
                fclose(fp);
                return EX_DATAERR;
            }
            // Bytes remaining in file
            size -= sizeof(ipheader.ipv6);
        }
        else
        {
            fprintf(stderr, "Invalid Ethernet Type");
            return EX_IOERR;
        }
        

        // Read in UDP Header
        read = fread(&udp, UDP_HEADER, 1, fp);
        if (read < 1)
        {
            fprintf(stderr, "Could not read UDP Header");
            fclose(fp);
            return EX_IOERR;
        }
        // checking for length mismatch in udp length to bytes total bytes
        // reminaing in file
        if (size < ntohs(udp.length))
        {
            fprintf(stderr, "Packet length mismatch."
                            " Not enough bytes left in file\n");
            fclose(fp);
            return EX_DATAERR;
        }
        // Bytes remaining in file
        size -= sizeof(udp);

        // Read in Zerg Header
        read = fread(&zerg, ZERG_HEADER, 1, fp);
        if (read < 1)
        {
            fprintf(stderr, "Could not read Zerg Packet Header");
            fclose(fp);
            return EX_IOERR;
        }
        int length = ((ntohl(zerg.length)) >> 8 & 0XFFFFFF);

        // checking for length mismatch in zerg length to bytes total bytes
        // reminaing in file
        if (size < length)
        {
            fprintf(stderr, "Packet length mismatch."
                            " Not enough bytes left in file\n");
            fclose(fp);
            return EX_DATAERR;
        }
        // Bytes remaining in file
        size -= length;

        int version = zerg.verType >> 4 & 0xF;
        int type = zerg.verType & 0xF;

        printf("Version   : %d\n", version);
        printf("Sequence  : %d\n", ntohl(zerg.seqID));
        printf("From      : %d\n", ntohs(zerg.sid));
        printf("To        : %d\n", ntohs(zerg.did));

        // Zerg header length - 12 bytes of fixed length header equals
        // the Zerg payload size.
        int payloadSize = length - ZERG_HEADER;
        int funcResult = 0;

        switch (type)
        {
        case 0:;
            // Payload is a message
            funcResult = zergMessage(fp, payloadSize);
            if (funcResult)
            {
                printf("Invlaid Zerg message format‽");
                fclose(fp);
                return EX_IOERR;
            }
            break;
        case 1:
            // Payload is a Zerg Status
            funcResult = zergStatus(fp, payloadSize);
            if (funcResult)
            {
                printf("Invlaid Zerg status format‽");
                fclose(fp);
                return EX_IOERR;
            }
            break;
        case 2:
            //Paylaod is a Command Instruction
            funcResult = zergCommand(fp);
            if (funcResult)
            {
                printf("Invlaid Zerg command format‽");
                fclose(fp);
                return EX_IOERR;
            }
            break;
        case 3:
            //Payload is GPS Data
            funcResult = zergGPS(fp, payloadSize);
            if (funcResult)
            {
                printf("Invlaid Zerg GPS format‽");
                fclose(fp);
                return EX_IOERR;
            }
            break;

        default:
            // Someone messed up, there is an unkown payload type‽
            printf("Uknown Payload Type‽");
            fclose(fp);
            return EX_IOERR;
        }

        // Ensuring the program has read the entire packet, if 
        // there was padding bytes added to ensure minimum packet size
        int padding = pHead.lenDataCap - TOTAL_HEADER_SIZE - length;
        if (padding != 0)
        {
            char *packetPadding = malloc(padding);

            read = fread(packetPadding, padding, 1, fp);
            if (read < 1)
            {
                fprintf(stderr, "Could not read padding");
                return EX_DATAERR;
            }
            // Bytes remaining in file
            size -= padding;

            free(packetPadding);
            packetPadding = NULL;
        }

        // Malloc-ing memory for buff step taken from:
        // stackoverflow.com/questions/28645473/segmentation-fault-in-comparing-
        // the-second-byte-of-two-files
        char *buff = (char *) malloc(sizeof(char));

        if (!buff)
        {
            fprintf(stderr, "Unable to malloc space");
            fclose(fp);
            return EX_DATAERR;
        }
        // Reading in next byte to see if at the EOF or not.
        // Need to read, feof will not recognize EOF if you just
        // fseek to the next byte.
        read = fread(buff, 1, 1, fp);
        if (read < 1 && !feof(fp))
        {
            fprintf(stderr, "Could not read End of File Test");
            return EX_DATAERR;
        }

        // Only needed the malloc to advance the file pointer a byte,
        // the Valgrind overloard wants be to free the malloc.
        free(buff);
        buff = NULL;

        if (feof(fp))
        {
            break;
        }
        // Need to move the pointer back one so the next packet reads 
        // in corectly.
        fseek(fp, -1, SEEK_CUR);

        // New line to seperate each indivudial Zerg Packet
        printf("\n");
    }
    // Closing the file upon coompletion
    fclose(fp);
    return 0;
}
