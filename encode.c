/**************************************************************************
        Classification: U/FOUO
*//**
    @file
    @brief Program used to encode into a Zerg Psychic CAPtures (PCAP)
           of network traffic.

    Program will encode data into a Zerg Psychic CAPtures (PCAP) of network 
    traffic.
***************************************************************************/
#define _BSD_SOURCE

#include <arpa/inet.h>

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sysexits.h>
#include <sys/stat.h>

#include "struct.h"
#include "encodefunc.h"

// Maximum size of line to be read in
enum
{MAX_LINE_BUFF = 128};

int
main(int argc, char *argv[])
{

    if (argc > 3)
    {
        // User done entered to many arguments, looks like I need to 
        // inform them how to run the program
        fprintf(stderr, "Usage: %s <Input File> [<Output File>]\n", argv[0]);
        return EX_IOERR;
    }

    // With this I can call stat on my input file to get file 
    // size in bytes
    struct stat fileStat;
    stat(argv[1], &fileStat);
    int size = fileStat.st_size;

    // Ensuring file is not empty (size == 0)
    if (size == 0)
    {
        fprintf(stderr, "Input File is of size 0\n");
        return EX_NOINPUT;
    }

    FILE *fileRead = fopen(argv[1], "rb");
    if (!fileRead)
    {
        printf("Unable to open file‽\n");
        return EX_IOERR;
    }


    char *fileOpen = NULL;
    if (argc == 2)
    {
        // I'll help the user if they forget to enter the output file name
        // calloc plus one to ensure null termination
        fileOpen = calloc((strlen("out.pcap")+1), sizeof(char));
        if (!fileOpen)
        {
            fprintf(stderr, "Unable to calloc input file name");
            free(fileRead);
            return EX_UNAVAILABLE;
        }
        strncpy(fileOpen, "out.pcap", strlen("out.pcap"));
    }
    else if (argc == 3)
    {
        // Calloc plus one to ensure null termiantion
        fileOpen = calloc((strlen(argv[2])+1),sizeof(char));
        if (!fileOpen)
        {
            fprintf(stderr, "Unable to calloc input file name");
            //free(fileRead);
            return EX_UNAVAILABLE;
        }
        strncpy(fileOpen, argv[2], strlen(argv[2]));
    }
    else
    {
        // Stops program if no arguments given i.e. argc == 1 or argc > 3.
        fprintf(stderr, "Usage: %s <Input File> [<Output File>]\n", argv[0]);
        free(fileRead);
        return EX_IOERR;
    }
    FILE *fileWrite = fopen(fileOpen, "wb");

    if (!fileWrite)
    {
        free(fileRead);
        return EX_CANTCREAT;
    }


    struct capHeader encodePcap = { 0 };
    struct packetHeader encodePacket = { 0 };
    struct ethFrame encodeEth = { 0 };
    struct udpHeader encodeUdp = { 0 };
    struct zergHeader zergEncode = { 0 };

    char *line = malloc(MAX_LINE_BUFF * sizeof(*line));
    if (!line)
    {
        fprintf(stderr, "Unable to malloc line buffer");
        free(fileRead);
        free(fileWrite);
        return 1;
    }
    char word[MAX_LINE_BUFF];
    char *err;
    char *token;

    // Encode any defualt values into the headers
    encodeDefaults(&encodePcap, &encodePacket, &encodeEth, &encodeUdp);

    // Write the pcap File Header now. Only needed once per 
    // encoding
    fwrite(&encodePcap, sizeof(encodePcap), 1, fileWrite);

    fgets(line, sizeof(word), fileRead);

    while (true)
    {
        token = strtok(line, ":\0");
        if (!(strncmp(token, "IP Version", strlen("IP Version"))))
        {
            token = strtok(NULL, ":\0");

            if (!(strncmp(token, " IPv4", strlen(" IPv4"))))
            {
                encodeEth.ethType = htons(0x0800);
                setIpv4Header(&ih.ipv4);
            }
            else if (!(strncmp(token, " IPv6", strlen(" IPv6"))))
            {
                encodeEth.ethType = htons(0x86DD);
                setIpv6Header(&ih.ipv6);
            }
            else
            {
                // Invalid ehternet type was read
                fprintf(stderr, "Invlaid Ethernet Type.\n");
                return EX_DATAERR;
            }
        }
        else
        {
            // Invalid ehternet type was read
            fprintf(stderr, "Invlaid Ethernet Type.\n");
            return EX_DATAERR;
        }
        // Storing Variables using pointer arithmatic
        // taken from Alex Dow
        fgets(line, sizeof(word), fileRead);
        uint8_t version = strtol(line + 11, &err, 10);
        zergEncode.verType = version << 4;

        fgets(line, sizeof(word), fileRead);
        zergEncode.seqID = htonl(strtol(line + 11, &err, 10));

        fgets(line, sizeof(word), fileRead);
        zergEncode.sid = htons(strtol(line + 11, &err, 10));

        fgets(line, sizeof(word), fileRead);
        zergEncode.did = htons(strtol(line + 11, &err, 10));

        fgets(line, sizeof(word), fileRead);

        token = strtok(line, ":\0");

        // Longitude == first line of gps payload
        if (!(strncmp(token, "Latatitude", strlen("Latatitude"))))
        {
            struct gpspayload gpsEncode;

            encodeGPS(&zergEncode, &gpsEncode, fileRead, line);

            zergEncode.length = sizeof(zergEncode) + sizeof(gpsEncode);
            writeHeaders(&encodePacket, &encodeEth, &ih, &encodeUdp,
                         &zergEncode, fileWrite);

            fwrite(&gpsEncode, sizeof(gpsEncode), 1, fileWrite);
        }
        // Message == first line of a message payload
        else if (!(strncmp(token, "Message", strlen("Message"))))
        {
            
            line = line + 12;
            char *zergMessage = calloc((strlen(line) + 1), sizeof(char));

            strncpy(zergMessage, line, strlen(line));

            zergEncode.length = sizeof(zergEncode) + (strlen(line) - 1);
            writeHeaders(&encodePacket, &encodeEth, &ih, &encodeUdp,
                         &zergEncode, fileWrite);

            fwrite(zergMessage, strlen(line) - 1, 1, fileWrite);

            while(fgets(line, sizeof(word), fileRead))
            {
                strncpy(zergMessage, line, strlen(line));
                if (!(strncmp(zergMessage, "\n", strlen("\n"))))
                {
                    break;
                }

                fwrite(zergMessage, strlen(line) - 1, 1, fileWrite);
            }
            free(zergMessage);
            line -= 12;
        }
        // Name == first line of status payload
        else if (!(strncmp(token, "Name", strlen("Name"))))
        {
            struct statuspayload zergStatus;

            line = line + 12;
            char *zergName = calloc((strlen(line) + 1), sizeof(char));

            strncpy(zergName, line, strlen(line) - 1);

            encodeStatus(&zergEncode, &zergStatus, fileRead, line);

            zergEncode.length = sizeof(zergEncode) + sizeof(zergStatus);
            zergEncode.length += strlen(zergName);
            zergEncode.length = zergEncode.length;

            writeHeaders(&encodePacket, &encodeEth, &ih, &encodeUdp,
                         &zergEncode, fileWrite);

            fwrite(&zergStatus, sizeof(zergStatus), 1, fileWrite);
            fwrite(zergName, strlen(zergName), 1, fileWrite);

            free(zergName);
            line -= 12;

        }
        // command == first line of command payload
        else if (!(strncmp(token, "Command", strlen("Command"))))
        {
            token = strtok(NULL, ":\0");
            zergEncode.verType = zergEncode.verType | 0x2;

            if (!(strncmp(token, " Get Status", strlen(" Get Status"))))
            {
                uint16_t command = 0;

                // Zerg Packet Length is size of header + payload 
                // command payload with no paramaters is 14 bytes long.
                zergEncode.length = ZERG_HEADER + 2;
                writeHeaders(&encodePacket, &encodeEth, &ih,
                             &encodeUdp, &zergEncode, fileWrite);
                fwrite(&command, 2, 1, fileWrite);
            }
            else if (!(strncmp(token, " Goto Location",
                       strlen(" Goto Location"))))
            {
                struct commandpayload commandEncode;

                commandEncode.command = htons(0x00000001);

                fgets(line, sizeof(word), fileRead);

                union
                {
                    uint32_t hex;
                    float num;
                } tmp;
                tmp.num = strtof(line + 12, NULL);
                // Bearing (parma2) is diplayed and stored into the text
                // file before distance (param1). Hence the out of order here
                commandEncode.param2 = tmp.hex;
                commandEncode.param2 = htonl(commandEncode.param2);

                fgets(line, sizeof(word), fileRead);
                commandEncode.param1 = htons(strtof(line + 11, NULL));

                // Zerg Header Length is size of header + payload 
                // command payload with all paramaters is 8 bytes long.
                zergEncode.length = ZERG_HEADER + 8;
                writeHeaders(&encodePacket, &encodeEth, &ih,
                             &encodeUdp, &zergEncode, fileWrite);
                fwrite(&commandEncode, sizeof(commandEncode), 1, fileWrite);
            }
            else if (!(strncmp(token, " Get GPS", strlen(" Get GPS"))))
            {
                uint16_t command = htons(0x00000002);

                // Zerg Packet Length is size of header + payload 
                // command payload with no paramaters is 14 bytes long.
                zergEncode.length = ZERG_HEADER + 2;
                writeHeaders(&encodePacket, &encodeEth, &ih,
                             &encodeUdp, &zergEncode, fileWrite);
                fwrite(&command, 2, 1, fileWrite);
            }
            else if (!(strncmp(token, " Return", strlen(" Return"))))
            {
                uint16_t command = htons(0x00000004);

                // Zerg Packet Length is size of header + payload 
                // command payload with no paramaters is 14 bytes long.
                zergEncode.length = ZERG_HEADER + 2;
                writeHeaders(&encodePacket, &encodeEth, &ih,
                             &encodeUdp, &zergEncode, fileWrite);
                fwrite(&command, 2, 1, fileWrite);

            }
            else if (!(strncmp(token, " Set Group", strlen(" Set Group"))))
            {
                struct commandpayload commandEncode;

                commandEncode.command = htons(0x00000005);
                fgets(line, sizeof(word), fileRead);
                token = strtok(line, ":\0");
                commandEncode.param1 = 1;
                if (!(strncmp(token, " Add", strlen(" Add"))))
                {
                    commandEncode.param1 = 0;
                }

                commandEncode.param1 = htons(commandEncode.param1);
                commandEncode.param2 = htonl(strtol(line + 17, &err, 10));

                // Zerg Header Length is size of header + payload 
                // command payload with all paramaters is 8 bytes long.
                zergEncode.length = ZERG_HEADER + 8;
                writeHeaders(&encodePacket, &encodeEth, &ih,
                             &encodeUdp, &zergEncode, fileWrite);
                fwrite(&commandEncode, sizeof(commandEncode), 1, fileWrite);
            }
            else if (!(strncmp(token, " Stop", strlen(" Stop"))))
            {
                uint16_t command = htons(0x00000006);

                // Zerg Packet Length is size of header + payload 
                // command payload with no paramaters is 14 bytes long.
                zergEncode.length = ZERG_HEADER + 2;
                writeHeaders(&encodePacket, &encodeEth, &ih,
                             &encodeUdp, &zergEncode, fileWrite);
                fwrite(&command, 2, 1, fileWrite);
            }
            else if (!(strncmp(token, " Repeat", strlen(" Repeat"))))
            {
                struct commandpayload commandEncode;

                commandEncode.command = htons(0x00000007);
                commandEncode.param1 = 0;
                fgets(line, sizeof(word), fileRead);
                commandEncode.param2 = htonl(strtol(line + 11, &err, 10));

                // Zerg Header Length is size of header + payload 
                // command payload with all paramaters is 8 bytes long.
                zergEncode.length = ZERG_HEADER + 8;
                writeHeaders(&encodePacket, &encodeEth, &ih,
                             &encodeUdp, &zergEncode, fileWrite);
                fwrite(&commandEncode, sizeof(commandEncode), 1, fileWrite);
            }
            else
            {
                 fprintf(stderr, "Unable to encode"
                                 " Command Payload Unknown Type\n");
                 freeMemory(fileRead, fileWrite, line, fileOpen);
                 return EX_IOERR;
            }
        }
        // If the data is bad pull the chute and bail from the plane
        else
        {
            fprintf(stderr, "Unable to encode"
                                 " Packet, Unknown Type\n");
            freeMemory(fileRead, fileWrite, line, fileOpen);
        }

        // Blasting through any newlines the user may have padded
        // between packets
        fgets(line, sizeof(word), fileRead);
        while ( strtok(line, "\n") == NULL)
        {
            fgets(line, sizeof(word), fileRead);
        }


        // Determine if we are at the end of the file 
        if (feof(fileRead))
        {
            break;
        }
    }
    // Outsource the task of freeing any memory
    freeMemory(fileRead, fileWrite, line, fileOpen);
    return 0;
}
