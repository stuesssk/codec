/**************************************************************************
            Classification: U/FOUO
*//**
    @file
    @brief contains functions necessary for encode.c to properly compile and
           execute
***************************************************************************/
#define _BSD_SOURCE

#include <arpa/inet.h>

#include <endian.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>

#include "encodefunc.h"
#include "struct.h"

/**************************************************************************
            getBreedId
*//**
    @brief  getBreedId returns the breed ID number associated with 
            a given zerg type
    @param  breed   zerg type in string format

    getBreedId accepts the string of the breed name and returns the 
    ID number accositaed with the zerg type.

    @return returns the integer value associated with the zerg type
***************************************************************************/
int
getBreedId(char *breed)
{
    // For loop construct for breed string name to ID #
    // From Alex Dow
    const char *breedName[16] = {
        "Overmind",
        "Larva",
        "Cerebrate",
        "Overlord",
        "Queen",
        "Drone",
        "Zergling",
        "Lurker",
        "Broodling",
        "Hydralisk",
        "Guardian",
        "Scourge",
        "Ultralisk",
        "Mutalisk",
        "Defiler",
        "Devourer"
    };

    for (size_t i = 0; i < sizeof(breedName); ++i)
    {
        if (!strcmp(breed, breedName[i]))
        {
            return i;
        }
    }
    return 0;
}

/**************************************************************************
            writeHeaders
*//**
    @brief  writeHeaders encodes the hex value of the default values 
            in the various packet headers
    @param  encodePacket    struct for pcap packet header
    @param  encodeEth       struct for Ethernet Frame
    @param  ipv4Header      struct for IPv4 packet header
    @param  encodeUdp       struct for UDP packet header
    @param  zergEncode      struct for Zerg packet header
    @param  fileWrite       file pointer to fiel to be written to   


    writeHeader accepts the srtuct pointers fro the pcap, ethernet, ipv4,
    udp, and zerg headers and writes the values as hex into the file 
    pointed to by filewrite

    @return returns void
***************************************************************************/
void
writeHeaders(struct packetHeader *encodePacket, struct ethFrame *encodeEth,
             union ipHeader *ih, struct udpHeader *encodeUdp,
             struct zergHeader *zergEncode, FILE * fileWrite)
{

    encodeUdp->length = zergEncode->length + sizeof(*encodeUdp);

    if (ntohs(encodeEth->ethType) == 0x0800)
    {
        ih->ipv4.length = encodeUdp->length + sizeof(ih->ipv4);
        encodePacket->lenDataCap = ih->ipv4.length + sizeof(*encodeEth);
    }
    else
    {
        ih->ipv6.payloadLen = encodeUdp->length;
        encodePacket->lenDataCap = ih->ipv6.payloadLen + sizeof(*encodeEth);
    }
    encodePacket->untruncatedLen = encodePacket->lenDataCap;

    fwrite(*(&encodePacket), sizeof(*encodePacket), 1, fileWrite);

    fwrite(*(&encodeEth), sizeof(*encodeEth), 1, fileWrite);
    if (ntohs(encodeEth->ethType) == 0x0800)
    {
        ih->ipv4.length = htons(ih->ipv4.length);

        // Calculate IPv4 Checksum
        unsigned int ipv4Checksum =
            (ih->ipv4.versionIhl << 8 | ih->ipv4.dscpEcn) +
            htons(ih->ipv4.length) + ih->ipv4.id + ih->ipv4.flagsFrag +
            (ih->ipv4.ttl << 8 | ih->ipv4.protocol) +
            (htonl(ih->ipv4.sIP) & 0xFFFF) + (htonl(ih->ipv4.sIP) >> 16) +
            (htonl(ih->ipv4.dIP) & 0xFFFF) + (htonl(ih->ipv4.dIP) >> 16);
        if (ipv4Checksum > 0xFFFF)
        {
            unsigned int tmp = ipv4Checksum >> 16;

            ipv4Checksum += tmp;
        }

        ih->ipv4.checksum = htons((~ipv4Checksum) & 0xFFFF);
        fwrite((&ih->ipv4), sizeof(ih->ipv4), 1, fileWrite);
    }
    else if (ntohs(encodeEth->ethType) == 0x86DD)
    {
        ih->ipv6.payloadLen = htons(ih->ipv6.payloadLen);
        fwrite((&ih->ipv6), sizeof(ih->ipv6), 1, fileWrite);
    }



    encodeUdp->length = htons(encodeUdp->length);

    // Calculate UDP Checksum
    int udpChecksum =
        htons(encodeUdp->length) + htons(encodeUdp->sPort) +
        htons(encodeUdp->sPort);
    if (udpChecksum > 0xFFFF)
    {
        int tmp = udpChecksum >> 16;

        udpChecksum += tmp;
    }
    encodeUdp->checksum = htons((~udpChecksum) & 0xFFFF);

    fwrite(*(&encodeUdp), sizeof(*encodeUdp), 1, fileWrite);

    zergEncode->length = htonl(zergEncode->length) >> 8;
    fwrite(*(&zergEncode), sizeof(*zergEncode), 1, fileWrite);
}

/**************************************************************************
            encodeGPS
*//**
    @brief  encodeGPS encodes the hex values for each parameter in the 
            GPS payload
    @param  zergEncode  struct for zerg packet header
    @param  gpsEncode   struct for GPS payload
    @param  line        last line read before stepping into function
    @param  fileRead    file pointer to file to be read from   


    encodeGPS accepts the struct pointers from the zerg header and the gps 
    payload and encodes the proper values in to the fields

    @return returns void
***************************************************************************/
void
encodeGPS(struct zergHeader *zergEncode, struct gpspayload *gpsEncode,
          FILE * fileRead, char *line)
{
    union
    {
        uint64_t hex;
        double num;
    } temp;

    union
    {
        uint32_t hex;
        float num;
    } tmp;

    char word[128];

    zergEncode->verType = zergEncode->verType | 0x3;
    line = line + 11;


    double latDeg = 0, latMin = 0, latSec = 0;

    latDeg = strtod(line + 1, NULL);
    latMin = strtod(line + 5, NULL);
    latSec = strtod(line + 10, NULL);
    char latDir = *(line + 21);
    double latitude = (latDeg + latMin / 60 + latSec / 3600);


    if (latDir == 'S')
    {
        latitude = latitude * -1;
    }
    temp.num = latitude;

    gpsEncode->latitude = htobe64(temp.hex);
    fgets(line, sizeof(word), fileRead);
    line = line + 11;

    double longDeg = 0, longMin = 0, longSec = 0;

    longDeg = strtod(line + 1, NULL);
    longMin = strtod(line + 5, NULL);
    longSec = strtod(line + 10, NULL);

    char longDir = *(line + 21);
    double longitude = (longDeg + longMin / 60 + longSec / 3600);

    if (longDir == 'W')
    {
        longitude = longitude * -1;
    }
    temp.num = longitude;

    gpsEncode->longitude = htobe64(temp.hex);

    fgets(line, sizeof(word), fileRead);
    line = line + 11;

    // conversion from meters to fathoms
    tmp.num = (strtof(line, NULL)) / 1.8288;
    gpsEncode->altitude = htonl(tmp.hex);

    fgets(line, sizeof(word), fileRead);
    line = line + 11;
    tmp.num = strtof(line, NULL);
    gpsEncode->bearing = htonl(tmp.hex);

    fgets(line, sizeof(word), fileRead);
    line = line + 11;

    // km/h to m/s converions
    tmp.num = strtof(line, NULL) / 3.6;
    gpsEncode->speed = htonl(tmp.hex);


    fgets(line, sizeof(word), fileRead);
    line = line + 11;

    // km/h to m/s converions
    tmp.num = strtof(line, NULL);
    gpsEncode->accuracy = htonl(tmp.hex);
}

/**************************************************************************
            encodeStatus
*//**
    @brief  encodeStatus encodes the hex values for each parameter in the 
            status payload
    @param  zergEncode  struct for zerg packet header
    @param  zergStatus  struct for status payload
    @param  line        last line read before stepping into function
    @param  fileRead    file pointer to file to be read from   


    encodeStatus accepts the struct pointers from the zerg header and the 
    status payload and encodes the proper values in to the fields


    @return returns void
***************************************************************************/
void
encodeStatus(struct zergHeader *zergEncode, struct statuspayload *zergStatus,
             FILE * fileRead, char *line)
{
    char word[128];
    char *err;

    union
    {
        uint32_t hex;
        float num;
    } tmp;

    zergEncode->verType = zergEncode->verType | 0x1;

    fgets(line, sizeof(word), fileRead);
    char *token;

    token = strtok(line, ":/");
    token = strtok(NULL, ":/");
    int hp = strtol(token, &err, 10);

    zergStatus->hp = htonl(hp) >> 8;

    token = strtok(NULL, ":/");
    zergStatus->maxHp = htonl(strtol(token, &err, 10)) >> 8;

    fgets(line, sizeof(word), fileRead);
    zergStatus->armor = strtol(line + 11, &err, 10);

    fgets(line, sizeof(word), fileRead);
    line = line + 12;
    char *breed = calloc((strlen(line) + 1), sizeof(char));

    strncpy(breed, line, strlen(line) - 1);
    zergStatus->breed = getBreedId(breed);

    fgets(line, sizeof(word), fileRead);
    tmp.num = strtof(line + 11, NULL);
    zergStatus->speed = htonl(tmp.hex);

    free(breed);
}

/**************************************************************************
            encodeDefaults
*//**
    @brief  encodeDefaults encodes the default hex values for each header 
            struct.
    @param  encodePcap      struct for pcap file header
    @param  encodePacket    struct for pcap packet header
    @param  encodeEth       struct for Ethernet Frame
    @param  ipv4Header      struct for IPv4 packet header
    @param  encodeUdp       struct for UDP packet header
    @param  zergEncode      struct for Zerg packet header
    @param  fileWrite       file pointer to fiel to be written to    


    encodeStatus accepts the struct pointers from the pcap fiel header,
    pcap packet header, ethernet frame, and ipv4, udp and zerg headers
    and encodes any default values into the various fields.


    @return returns void
***************************************************************************/
void
encodeDefaults(struct capHeader *encodePcap, struct packetHeader *encodePacket,
               struct ethFrame *encodeEth, struct udpHeader *encodeUdp)
{
    // Default pcap File Header Values
    encodePcap->fileID = 0xa1b2c3d4;
    encodePcap->majorVer = 0x0002;
    encodePcap->minorVer = 0x0004;
    encodePcap->GMTOffset = 0x00000000;
    encodePcap->delta = 0x00000000;
    encodePcap->maxLength = 0x10000;
    encodePcap->layerType = 0x1;

    // Obtaining epoch and μseconds offset
    struct timeval time;

    gettimeofday(&time, NULL);

    // Default pcap Pakcet Header Values
    encodePacket->unixEpoch = htonl(time.tv_sec);
    encodePacket->usEpoch = htonl(time.tv_usec);
    encodePacket->lenDataCap = 0x0;
    encodePacket->untruncatedLen = 0x0;

    // Default Ethernet Fram Values
    encodeEth->dmac = 0x111111111111;
    encodeEth->smac = 0x222222222222;
    encodeEth->ethType = 8;

    // Default UDP Header Values
    encodeUdp->sPort = htons(0x1234);
    encodeUdp->dPort = htons(0x0EA7);
    encodeUdp->checksum = 0x0;
}

/**************************************************************************
            freememory
*//**
    @brief  freememory frees the open mallacs and closes the file pointers
    @param  fileRead    File pinter to file opened for reading
    @param  fileWrite   File pinter to file opened for writing
    @param  line        malloc used to read lines from fileRead
    @param  fileOpen    malloc used to store argv[2] or predefined output

    encodeStatus accepts the struct pointers from the pcap fiel header,
    pcap packet header, ethernet frame, and ipv4, udp and zerg headers
    and encodes any default values into the various fields.


    @return returns void
***************************************************************************/
void
freeMemory(FILE * fileRead, FILE * fileWrite, char *line, char *fileOpen)
{
    // Free all the mallocs the Valgrind overlord commands it
    free(line);
    free(fileOpen);
    // Close the file handles the Valgrind overlord commands it
    fclose(fileWrite);
    fclose(fileRead);
}

void
setIpv4Header(struct ipv4Header *encodeIpv4)
{
    // Default IPv4 Header Values
    encodeIpv4->versionIhl = 0x45;
    encodeIpv4->dscpEcn = 0;
    encodeIpv4->length = 0x0;
    encodeIpv4->id = 0x0;
    encodeIpv4->flagsFrag = 0x0;;
    encodeIpv4->ttl = 0x80;
    encodeIpv4->protocol = 0x11;
    encodeIpv4->checksum = 0x0;
    encodeIpv4->sIP = htonl(0xa1b2c3d4);
    encodeIpv4->dIP = htonl(0xc0a80001);
}

void
setIpv6Header(struct ipv6Header *encodeIpv6)
{
    // Default IPv4 Header Values
    encodeIpv6->verClassFlow = 0x60000000;
    encodeIpv6->payloadLen = 0x0;
    encodeIpv6->nextHeader = 0x11;
    encodeIpv6->hopLimit = 0x40;
    encodeIpv6->source1 = htobe64(0xfe80000000000000);
    encodeIpv6->source2 = htobe64(0xf8163efffe1cacfd);
    encodeIpv6->dest1 = htobe64(0xfe80000000000000);
    encodeIpv6->dest2 = htobe64(0xf8163efffe1cacfe);
}
