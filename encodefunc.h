#ifndef ENCODEFUNC_H
#define ENCODEFUNC_H


#include <stdio.h>

#include "struct.h"

int
getBreedId(char *breed);

void
writeHeaders(struct packetHeader *encodePacket, struct ethFrame *encodeEth,
             union ipHeader *ih, struct udpHeader *encodeUdp,
             struct zergHeader *zergEncode, FILE * fileWrite);

void
encodeGPS(struct zergHeader *zergEncode, struct gpspayload *gpsEncode,
          FILE *fileRead, char *line);

void
encodeStatus(struct zergHeader *zergEncode, struct statuspayload *zergStatus,
             FILE *fileRead, char *line);

void
encodeDefaults(struct capHeader *encdoePcap, struct packetHeader *encodePacket,
               struct ethFrame *encodeEth, struct udpHeader *encodeUdp);

void
freeMemory(FILE *fileRead, FILE *fileWrite, char *line, char *fileOpen);

void
setIpv4Header( struct ipv4Header *encodeIpv4);

void
setIpv6Header( struct ipv6Header *encodeIpv6);

#endif
