/**************************************************************************
      Classification: U/FOUO
*//**
    @file
    @brief contains functions necessary for decode.c to properly compile and
           execute
***************************************************************************/

#include <arpa/inet.h>

#include <math.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "zergdata.h"
#include "struct.h"

/**************************************************************************
      ntonll
*//**
    @brief  ntonll converts a uint64_t number from little endian to 
            big endian
    @param  value   the unit64_t number needing conversion

    ntonll accepts a unit64_t value. First checks the endianness of the
    system. If the system is little endian, it converts to big endian by 
    using ntohl on each 32-bit section and concatinating both before
    returning the value.

    @return returns the converted unint64_t value 
***************************************************************************/
uint64_t
ntonll(uint64_t value)
{
    // uint64_t conversion from little endian to big endian, taken from:
    // stackoverflow.com/questions/3022552/is-there-any-standard-htonl-like-
    // function-for-64-bits-integers-in-c
    int num = 42;

    // Testing Endianness 
    if (*(char *) &num == 42)
    {
        // swap if Little Endian
        return ((uint64_t) ntohl(value & 0xFFFFFFFF) << 32LL |
                ntohl(value >> 32));
    }
    else
    {
        // Return if Big Endian already
        return value;
    }
}

/**************************************************************************
      zergStatus
*//**
    @brief  extracts Zerg Status data
    @param  fp       FILE pointer that is being read in
    @param  length   length of zerg packet

    zergStatus accepts the file pointer of the file being read in and the 
    total length of the zerg packet and prints the Zerg statistics
    encoded into the packet (Name, HP, Type, Armor, MaxSPeed)

    @return void
***************************************************************************/
int
zergStatus(FILE * fp, int length)
{
    struct statuspayload zergStatus;

    int read = fread(&zergStatus, ZERG_HEADER , 1, fp);

    if (read < 1)
    {
        perror("Could not read Zerg Status Data");
    }

    unsigned int armor = zergStatus.armor;

    unsigned int breed = zergStatus.breed;

    unsigned int maxHp = (ntohl(zergStatus.maxHp) >> 8 & 0xFFF);

    int hp = (ntohl(zergStatus.hp) >> 8 & 0xFFF);

    // Union used to convert binary 32 into a decimal number
    // Union conversion taken from:
    // stackoverflow.com/questions/28257524/convert-iee-754-floating-point-
    // back-to-decimal-c-language
    union
    {
        uint32_t hex;
        float num;
    } tmp;

    tmp.hex = ntohl(zergStatus.speed);
    double speed = tmp.num;

    // Zerg header of 12 bytes and status payload of fixed 12 bytes
    // not counting zerg name.  length - 23 ensures null terminated
    char *name = calloc((length - 11), sizeof(char));

    fread(name, (length - ZERG_HEADER ), 1, fp);
    if (read < 1)
    {
        perror("Could not read Zerg Status Data");
    }
    printf("Name      : %s\n", name);

    printf("HP        : %d/%d\n", hp, maxHp);

    printf("Armor     : %d\n", armor);

    char *zergBreed = calloc(MAX_ZERG_BREED, sizeof(char));

    printf("Breed     : ");
    switch (breed)
    {
    case 0:
        strncpy(zergBreed, "Overmind", sizeof("Overmind"));
        break;
    case 1:
        strncpy(zergBreed, "Larva", sizeof("larva"));
        break;
    case 2:
        strncpy(zergBreed, "Cerebrate", sizeof("Cerebrate"));
        break;
    case 3:
        strncpy(zergBreed, "Overlord", sizeof("Overlord"));
        break;
    case 4:
        strncpy(zergBreed, "Queen", sizeof("Queen"));
        break;
    case 5:
        strncpy(zergBreed, "Drone", sizeof("Drone"));
        break;
    case 6:
        strncpy(zergBreed, "Zergling", sizeof("Zergling"));
        break;
    case 7:
        strncpy(zergBreed, "Lurker", sizeof("Lurker"));
        break;
    case 8:
        strncpy(zergBreed, "Brooding", sizeof("Brooding"));
        break;
    case 9:
        strncpy(zergBreed, "Hydralisk", sizeof("Hydralisk"));
        break;
    case 10:
        strncpy(zergBreed, "Guardian", sizeof("Guardian"));
        break;
    case 11:
        strncpy(zergBreed, "Scourge", sizeof("Scourge"));
        break;
    case 12:
        strncpy(zergBreed, "Ultralisk", sizeof("Ultralisk"));
        break;
    case 13:
        strncpy(zergBreed, "Mutalisk", sizeof("Mutalisk"));
        break;
    case 14:
        strncpy(zergBreed, "Defiler", sizeof("Defiler"));
        break;
    case 15:
        strncpy(zergBreed, "Devourer", sizeof("Devourer"));
        break;
    default:
        return 1;
    }

    printf("%s\n", zergBreed);
    printf("MaxSpeed  : %lf m/s\n", speed);


    free(zergBreed);
    free(name);

    return 0;
}

/**************************************************************************
      zergCommand
*//**
    @brief  extracts Zerg Command data
    @param  fp       FILE pointer that is being read in
    @param  length   length of zerg packet

    zergStatus accepts the file pointer of the file being read in and the 
    total length of the zerg packet and prints the Zerg command
    encoded into the packet (command and optional paramater1/paramter2)

    @return int     return 0 on success, 1 on error.
***************************************************************************/
int
zergCommand(FILE * fp)
{
    // Union used to convert binary 32 into a decimal number
    // Union conversion taken from:
    // stackoverflow.com/questions/28257524/convert-iee-754-floating-point-
    // back-to-decimal-c-language
    union
    {
        uint32_t hex;
        float num;
    } tmp;

    uint16_t command;

    int read = fread(&command, 2, 1, fp);
    command = htons(command); 

    if (read < 1)
    {
        printf("Could not read Command Data");
        return 1;
    }

    // Exit function if Reserved Command Status is sent. Status 
    // should not be used. Reading padding will handle any bytes that
    // may have been sent with this reserved command.
    printf("Command   : ");
    if (command == 3)
    {
        printf("Reserved Command Status Used");
        return 1;
    }

    uint16_t param1;
    uint32_t param2;
    if (command % 2)
    {
        
        fread(&param1, 2, 1, fp);
        fread(&param2, 4, 1, fp);
    }


    switch (command)
    {
    case 0:
        printf("Get Status\n");
        break;
    case 1:;
        unsigned int distance = ntohs(param1);

        tmp.hex = ntohl(param2);
        double bearing = tmp.num;

        printf("Goto Location\n");
        printf("Bearing   : %lf°\n", bearing);
        printf("Distance  : %d m\n", distance);
        break;
    case 2:
        printf("Get GPS\n");
        break;
    // Handle Case 3 (Reserved Command) before switch case
    case 4:
        printf("Return to Hive\n");
        break;
    case 5:
        printf("Set Group\n");
        if (param1)
        {
            printf("Add To     : ");
        }
        else
        {
            printf("Remove From: ");
        }
        int groupID = ntohl(param2);

        printf("%d\n", groupID);
        break;
    case 6:
        printf("Stop\n");
        break;
    case 7:
        printf("Repeat\n");
        int sequenceID = ntohl(param2);

        printf("Sequence ID: %d\n", sequenceID);
        break;
    default:
        printf("Unknown command sent‽\n");
        return 1;
    }
    return 0;
}

/**************************************************************************
      zergGPS
*//**
    @brief  extracts Zerg GPS data
    @param  fp       FILE pointer that is being read in
    @param  length   length of zerg packet

    zergGPS accepts the file pointer of the file being read in and the 
    total length of the zerg packet and prints the Zerg GPS
    encoded into the message(Latitude, longitude, altitude, bearing, speed,
    and accuracy).

    @return void
***************************************************************************/
int
zergGPS(FILE * fp, int payloadSize)
{
    // Union used to convert binary 32 into a decimal number
    // Union conversion taken from:
    // stackoverflow.com/questions/28257524/convert-iee-754-floating-point-
    // back-to-decimal-c-language
    union
    {
        uint32_t hex;
        float num;
    } tmp;

    struct gpspayload gpsData;

    int read = fread(&gpsData, (payloadSize), 1, fp);

    if (read < 1)
    {
        return 1;
    }

    union
    {
        uint64_t hex;
        double num;
    } temp;

    temp.hex = ntonll(gpsData.latitude);
    double latitude = temp.num;
    char latChar = 'N';
    char longChar = 'E';

    if (latitude < 0)
    {
        latChar = 'S';
        latitude = fabs(latitude);
    }


    double latSec = latitude * 3600;
    int latDeg = latSec / 3600;

    latSec -= latDeg * 3600;
    int latMin = latSec / 60;

    latSec -= latMin * 60;

    temp.hex = ntonll(gpsData.longitude);
    double longitude = temp.num;

    if (longitude < 0)
    {
        longChar = 'W';
        longitude = fabs(longitude);
    }

    double longSec = longitude * 3600;
    int longDeg = longSec / 3600;

    longSec -= longDeg * 3600;
    int longMin = longSec / 60;

    longSec -= longMin * 60;

    printf("Latatitude: %d° %d' %.7lf\" %c\n", latDeg, latMin, latSec,
           latChar);

    printf("Longitude : %d° %d' %.7lf\" %c\n", longDeg, longMin, longSec,
           longChar);

    // Conversion from fathoms to meters meters = 1.8288 fathoms
    tmp.hex = ntohl(gpsData.altitude);
    double altitude = tmp.num * 1.8288;

    printf("Altitude  : %lf m\n", altitude);

    tmp.hex = ntohl(gpsData.bearing);
    double bearing = tmp.num;

    printf("Bearing   : %lf°\n", bearing);

    tmp.hex = ntohl(gpsData.speed);
    //conversion from m/s to km/h is *3600 sec/hr * 1km/1000m
    // which boils down to *3.6 (km sec)/(hr m) conversion factor
    double zurgSpeed = tmp.num * 3.6;

    printf("MaxSpeed  : %.2lf km/hr\n", zurgSpeed);

    tmp.hex = ntohl(gpsData.accuracy);
    printf("Accuracy  : %g m\n", tmp.num);

    return 0;
}

/**************************************************************************
      zergMessage
*//**
    @brief  extracts Zerg Message data
    @param  fp       FILE pointer that is being read in
    @param  length   length of zerg packet

    zergGPS accepts the file pointer of the file being read in and the 
    total length of the zerg packet and prints the Zerg Message
    encoded into the packet

    @return void
***************************************************************************/
int
zergMessage(FILE * fp, int payloadSize)
{
    // Adding 1 to apyloadSize ensures string is NULL terminated
    char *payload = calloc((payloadSize + 1), sizeof(char));

    int read = fread(payload, (payloadSize), 1, fp);

    if (read < 1)
    {
        return 1;
    }

    printf("Message   : %s\n", payload);
    free(payload);

    return 0;
}
