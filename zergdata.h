#ifndef ZERGDATA_H
#define ZERGDATA_H

#include <stdio.h>

#include "struct.h"

uint64_t
ntonll(uint64_t value);

int
zergStatus(FILE * fp, int length);

int
zergCommand(FILE * fp);

int
zergGPS(FILE * fp, int payloadSize);

int
zergMessage(FILE * fp, int payloadSize);

#endif
